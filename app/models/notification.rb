class Notification < ApplicationRecord
  after_create_commit {
    NotificationBroadcastJob.perform_later(Notification.count,self)
    p self
  }
  validates :event, presence: true
end
